require("CardType")
Card = {}
Card.__index = Card

function Card:new(card_type)
    --Have no clue what this stuff does
    o = {}
    setmetatable(o, self)
    
    o.type = card_type
    return o
end

function Card:harvest(harvest_count)
    return self.type:harvest(harvest_count);
end