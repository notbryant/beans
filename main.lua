require("Card")
require("Player")

-- Declaration of the Number of cards in the deck
  -- 3 players = remove Cocoa
  -- 4-5 players = remove Coffee
  -- 6-7 players = remove Cocoa and Garden
COFFEE_N = 24
WAX_N = 22
BLUE_N = 20
CHILI_N = 18
STINK_N = 16
GREEN_N = 14
SOY_N = 12
BLACK_N = 10
RED_N = 8
GARDEN_N = 6
COCOA_N = 4

-- Declaration of the requirements for harvesting each type
COFFEE_C = {4,7,10,12}
WAX_C = {4,7,9,10}
BLUE_C = {4,6,8,10}
CHILI_C = {3,6,8,9}
STINK_C = {3,5,7,8}
GREEN_C = {3,5,6,7}
SOY_C = {2,4,6,7}
BLACK_C = {2,4,5,6}
RED_C = {2,3,4,5}
GARDEN_C = {2,2,3,3}
COCOA_C = {2,2,3,4}


cards = {}
players = {}
cardA = nil
cardB = nil
draw_cards = 0

OPPONENT_Y = 30
DECK_X = 210
DECK_Y = 160
HAND_Y = 400
CARD_W = 100
CARD_H = 157
SCARD_W = 50
FIELD_X = 550
SCARD_H = SCARD_W*(CARD_H/CARD_W)
A_X = DECK_X+10+(CARD_W*.70)+15
A_Y = DECK_Y+20
B_X = A_X+CARD_W+15
B_Y = A_Y




function love.load()
  --!(NUM_OF_PLAYERS<3 or NUM_OF_PLAYERS>7)
  NUM_OF_PLAYERS = 7

  --Get the correct deck count
  DECK_COUNT = WAX_N + BLUE_N + CHILI_N 
	      + STINK_N + GREEN_N + SOY_N + BLACK_N + RED_N 
  if(NUM_OF_PLAYERS ~=4 and NUM_OF_PLAYERS ~=5) then
      DECK_COUNT= DECK_COUNT + COFFEE_N
  else
      DECK_COUNT= DECK_COUNT + COCOA_N
  end
  if(NUM_OF_PLAYERS ~=6 and NUM_OF_PLAYERS ~=7) then
      DECK_COUNT= DECK_COUNT + GARDEN_N
  end

  --Add the correct cards to the deck
    --Coffee
      if(NUM_OF_PLAYERS ~=4 and NUM_OF_PLAYERS ~=5) then
	  local Coffee = CardType:new("Coffee",COFFEE_N,COFFEE_C)
	  for i=1,COFFEE_N do
	    table.insert(cards, Coffee)  --Card:new(Coffee)
	  end
      end
    --Wax
	local Wax = CardType:new("Wax",WAX_N,WAX_C)
      for i=1,WAX_N do
	table.insert(cards, Wax)
      end
    --Blue
	local Blue = CardType:new("Blue",BLUE_N,BLUE_C)
      for i=1,BLUE_N do
	table.insert(cards, Blue)
      end
    --Chili
	local Chili = CardType:new("Chili",CHILI_N,CHILI_C)
      for i=1,CHILI_N do
	table.insert(cards, Chili)
      end
    --Stink
	local Stink = CardType:new("Stink",STINK_N,STINK_C)
      for i=1,STINK_N do
	table.insert(cards, Stink)
      end
    --Green
	local Green = CardType:new("Green",GREEN_N,GREEN_C)
      for i=1,GREEN_N do
	table.insert(cards, Green)
      end
    --Soy
	local Soy = CardType:new("Soy",SOY_N,SOY_C)
      for i=1,SOY_N do
	table.insert(cards, Soy)
      end
    --Black
	local Black = CardType:new("Black-eyed",BLACK_N,BLACK_C)
      for i=1,BLACK_N do
	table.insert(cards, Black)
      end
    --Red
	local Red = CardType:new("Red",RED_N,RED_C)
      for i=1,RED_N do
	table.insert(cards, Red)
      end
  
  --Create the players
  players[1] = Player:new("Bryant")
  for i=2, NUM_OF_PLAYERS do
    players[i] = Player:new("Player " .. i)
  end
    cards = shuffle(cards)
    --Deal the cards
    for i=1, 5 do
      for j=2, NUM_OF_PLAYERS do
	Hand.pushend(players[j].hand, cards[1])
	table.remove(cards, 1)
      end
    end
    draw_cards = 5
    
    --Set up for the draw animations
    final_position_x = (#players[1].hand+1)*50 + 10
    anim_x = final_position_x
    final_position_y = HAND_Y
    anim_y = final_position_y
end

function shuffle( a )
        local c = #a
        math.randomseed( os.time() )
        for i = 1, (c * 20) do
                local ndx0 = math.random( 1, c )
                local ndx1 = math.random( 1, c )
                local temp = a[ ndx0 ]
                a[ ndx0 ] = a[ ndx1 ]
                a[ ndx1 ] = temp
        end
    return a
end

turn_counter=#players
phase_counter = 1
--1 = play first card
--2 = ask play second card
--(2.5) = draw two cards
--3 = trading, wait until "finish turn" is clicked"
--(3.5) = draw three cards
--4 = increment turn_counter

  --TODO: Shuffle animation

transit_field=0
function love.update(dt)
  --This block contains everything needed for drawing cards
  -- both to the end of the hand, and to the middle
  if draw_cards>0 then
      if anim_x==final_position_x and anim_y==final_position_y then
	  if draw_to_middle then
	      final_position_x= DECK_X + 100
	      final_position_y = DECK_Y
	  else
	      final_position_x = (#players[1].hand+1)*50 + 10
	      final_position_y = HAND_Y
	  end
	  anim_x=DECK_X
	  anim_y=DECK_Y
	  status_text = "Drawing a card"
      end
      if draw_to_middle then
	anim_x = anim_x+dt*300
	anim_y = anim_y+dt*100
      else
	anim_x = anim_x-dt*300
	anim_y = anim_y+dt*500
      end
      --This is when the animation is completed
      if anim_x<final_position_x then anim_x = final_position_x end
      if anim_y>final_position_y then anim_y = final_position_y end
      if(anim_x==final_position_x and anim_y==final_position_y) then
	  if draw_to_middle then
	    if draw_cards==1 then
	      cardA = cards[1]
	    else
	      cardB = cards[1]
	    end
	  else
	    Hand.pushend(players[1].hand, cards[1])
	  end
	  table.remove(cards, 1)
	  draw_cards = draw_cards-1
      end
      
  --This next section covers the first 2 phases
  elseif phase_counter<3 then
    if(phase_counter==1) then
	status_text = "Choose where to plant your first card"
    else
	status_text = "Either plant your second card or click the deck to draw"
    end
    
    
      --This block takes care of animations to the fields
      --TODO: Should this be in love.mouse.whatever?
      if transit_field==0 then
	  if anim_x==final_position_x and anim_y==final_position_y 
	      and love.mouse.isDown("l") then
	      local mx, my = love.mouse.getPosition()
		local x = FIELD_X
	      local y = HAND_Y+CARD_H*.2*.7
	      transit_field = getField(mx, my)
	      
	      --Deck
	      if phase_counter==2 and inside(mx, my, DECK_X, DECK_Y, CARD_W*0.7, CARD_H*0.7) then
		  draw_cards = 2
		  draw_to_middle = true
		  phase_counter = phase_counter + 1
	      elseif transit_field>0 then
		anim_y = HAND_Y
		anim_X = 10
		transit_card = Hand.popstart(players[1].hand)
	      end
      end
    else
      --Update the animation
	    anim_x = anim_x+dt*1000
	    anim_y = anim_y+dt*100
	    if anim_x>final_position_x then anim_x = final_position_x end
	    if anim_y>final_position_y then anim_y = final_position_y end
	    if(anim_x==final_position_x and anim_y==final_position_y) then
		Player:plant(players[1], transit_field, transit_card)
		transit_field=0
		if phase_counter==2 then
		  draw_cards = 2
		  draw_to_middle = true
		end
		phase_counter = phase_counter + 1
	    end
	end
	
    --Enter the trading phase
    elseif phase_counter==3 then
	status_text = "The floor is open to trade with you"
	
      --This block takes care of animations to the fields
      if transit_field==0 then
	  if anim_x==final_position_x and anim_y==final_position_y 
	      and love.mouse.isDown("l") then
	--Handles selecting one of the two
	      local mx, my = love.mouse.getPosition()
	      local x = DECK_X+10+(CARD_W*.70)+15
	      local y = DECK_Y + 20
	      local w = CARD_W
	      local h = CARD_H
	      --First card
	      if(inside(mx, my, x, y, w, h)) then
		  selected="A"
	      end
	      --Second card
	      x = x + CARD_W + 15
	      if(inside(mx, my, x, y, w, h)) then
		  selected="B"
	      end
	      x = A_X+10
	      y = A_Y+CARD_H+10
	      w = CARD_W*2
	      h = 20
	      --Finish
	      if(inside(mx, my, x, y, w, h)) then
		draw_cards = 3
		draw_to_middle = false
		phase_counter = phase_counter + 1
		    final_position_x = (#players[1].hand+1)*50 + 10
		    anim_x = final_position_x
		    final_position_y = HAND_Y
		    anim_y = final_position_y
		return
	      end
	--Handles selecting a field
	      transit_field = getField(mx, my)
	      if transit_field>0 then
		x = FIELD_X
		y = HAND_Y+CARD_H*.2*.7
  
		anim_y = HAND_Y
		anim_X = 10
		if selected=="A" then
		    transit_card = cardA
		    anim_x = A_X
		    anim_y = A_Y
		    cardA = nil
		    selected=""
		elseif selected=="B" then
		    transit_card = cardB
		    anim_x = B_X
		    anim_y = B_Y
		    cardB = nil
		    selected=""
		end
	      end
	  end
  --
      else
	  anim_x = anim_x+dt*300
	  anim_y = anim_y+dt*500
	  if anim_x>final_position_x then anim_x = final_position_x end
	  if anim_y>final_position_y then anim_y = final_position_y end
	  if(anim_x==final_position_x and anim_y==final_position_y) then
	      Player:plant(players[1], transit_field, transit_card)
	      transit_field=0
	  end
	
	
	--Wait for click on "Finish" button
	    --Must have planted/traded both cards
	--phase_counter = phase_counter + 1
      end
    elseif(phase_counter==4) then
	-- turn_counter = turn_counter+1
	--phase_counter = 1
  end
  
end

function getField(mx, my)
    local x = FIELD_X
    local y = HAND_Y+CARD_H*.2*.7
    local w = CARD_W*0.7
    local h = CARD_H*0.7
    butts = 0
    
    --First field
    if(inside(mx, my, x, y, w, h)) then
	final_position_x = x
	final_position_y = y
	butts=1
    end
    --Second field
    x = x + w + 10
    if(inside(mx, my, x, y, w, h)) then
	final_position_x = x
	final_position_y = y
	butts=2
    end
    --Third field
    x = x + w + 10
    if(players[1].third_field or NUM_OF_PLAYERS<=3) then
	if(inside(mx, my, x, y, w, h)) then
	    final_position_x = x
	    final_position_y = y
	    butts=3
	end
    end
    return butts
end



function love.draw()
    love.graphics.setBackgroundColor(54, 130, 0)
    
    
    img = love.graphics.newImage("images/Back.png")
    --Draw deck
    love.graphics.draw(img, DECK_X, DECK_Y, 0, .70, .70)
    love.graphics.rectangle("line", DECK_X, DECK_Y+CARD_H*.70, CARD_W*0.7, CARD_H*.70)
    love.graphics.rectangle("line", DECK_X+CARD_W*.70+10, DECK_Y, CARD_W*2+50, 2*CARD_H*.70)
      --Finish
    if phase_counter==3 and cardA==nil and cardB==nil then
	love.graphics.rectangle("fill", A_X+10, A_Y+CARD_H+10, CARD_W*2, 20)
	love.graphics.setColor(0, 0, 0)
	love.graphics.print("Finish", A_X + 90, A_Y+CARD_H+10 + 3)
	love.graphics.setColor(255, 255, 255)
    end
    if cardA~=nil then
      img = love.graphics.newImage("images/" .. cardA.name .. ".png")
      love.graphics.draw(img, A_X, A_Y)
      if selected=="A" then
	love.graphics.rectangle("line", A_X-5, A_Y-5, CARD_W+10, CARD_H+10)
      end
    end
    if cardB~=nil then
      img = love.graphics.newImage("images/" .. cardB.name .. ".png")
      love.graphics.draw(img, B_X, B_Y)
      if selected=="B" then
	love.graphics.rectangle("line", B_X-5, B_Y-5, CARD_W+10, CARD_H+10)
      end
    end
    
    --Draw opponents
    for i=2, #players do
      if i==6 then break end
      x = 20 + (i-2)*200
      y = OPPONENT_Y
      love.graphics.print(players[i].name, x, y)
      --Opponent fields
      love.graphics.rectangle("line", x, y+25, SCARD_W, SCARD_H)
      love.graphics.rectangle("line", x+SCARD_W+5, y+25, SCARD_W, SCARD_H)
      if(players[i].third_field or NUM_OF_PLAYERS==3) then
	love.graphics.rectangle("line", x+SCARD_W*2+5*2, y+25, SCARD_W, SCARD_H)
      end
      
      love.graphics.setColor(0, 0, 0)
      love.graphics.print(#players[i].hand+1, x+80, y+-20)
      love.graphics.setColor(255, 255, 255)
    end
    --TODO: Add players 6 and 7
    
    --Draw player's hand
    i=0
    for j=players[1].hand.first, players[1].hand.last do
      img = love.graphics.newImage("images/" .. players[1].hand[j].name .. ".png")
      love.graphics.draw(img, 10+50*i, HAND_Y)
      i=i+1
    end
    love.graphics.print(players[1].name, 10, HAND_Y+175)
    
    love.graphics.print(status_text, 300, HAND_Y+175)
    
    --Draw player's fields
    local mx, my = love.mouse.getPosition()
    local x = FIELD_X
    local y = HAND_Y+CARD_H*.2*.7
    local w = CARD_W*0.7
    local h = CARD_H*0.7
    local box_type = "line"
    
    --Field 1
    if((phase_counter<=2) and inside(mx, my, x, y, w, h)) then
	box_type = "fill"
    end
    if players[1].fields[1].count>0 then
	moveimg = love.graphics.newImage("images/" .. players[1].fields[1].type.name .. ".png")
	love.graphics.draw(moveimg, x, y, 0, .7, .7)
    else
	love.graphics.rectangle(box_type, x, y, w, h)
    end
    love.graphics.print(players[1].fields[1].count, x, y+CARD_H*.7+10)
    --Field 2
    box_type = "line"
    x = x + w + 10
    if((phase_counter<=2) and inside(mx, my, x, y, w, h)) then
	box_type = "fill"
    end
    if players[1].fields[2].count>0 then
	moveimg = love.graphics.newImage("images/" .. players[1].fields[2].type.name .. ".png")
	love.graphics.draw(moveimg, x, y, 0, .7, .7)
    else
	love.graphics.rectangle(box_type, x, y, w, h)
    end
    love.graphics.print(players[1].fields[2].count, x, y+CARD_H*.7+10)
    --Field 3
    box_type = "line"
    x = x + w + 10
    if(players[1].third_field or NUM_OF_PLAYERS<=3) then
      if((phase_counter<=2) and inside(mx, my, x, y, w, h)) then
	box_type = "fill"
      end
      if players[1].fields[3].count>0 then
	moveimg = love.graphics.newImage("images/" .. players[1].fields[3].type.name .. ".png")
	love.graphics.draw(moveimg, x, y, 0, .7, .7)
      else
	  love.graphics.rectangle(box_type, x, y, w, h)
      end
      love.graphics.print(players[1].fields[3].count, x, y+CARD_H*.7+10)
    end
    
    
    --Animations
    if draw_cards>0 then
	moveimg = love.graphics.newImage("images/Back.png")
	love.graphics.draw(moveimg, anim_x, anim_y)
    end
    if transit_card and transit_field>0 then
        moveFile = transit_card.name .. ".png"
	moveimg = love.graphics.newImage("images/" .. moveFile)
	love.graphics.draw(moveimg, anim_x, anim_y)
    end
end


--- ETC ---
function inside(mx, my, x, y, w, h)
    return mx >= x and mx <= (x+w) and my >= y and my <= (y+h)
end
